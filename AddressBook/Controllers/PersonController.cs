﻿using AddressBook.Models;
using AddressBook.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AddressBook.Controllers
{
    public class PersonController : BaseController
    {

        AddressBookEntities db = new AddressBookEntities();
        // GET: Person
        public ActionResult Index()
        {
            List<Person> person = db.Person.ToList();
            return View(person);
        }
        public ActionResult PersonAdd()
        {
            return View();
        }
         
        [HttpPost]
        public ActionResult PersonAdd(Person person)
        {

            person.UserId = Convert.ToInt32( Session["Id"].ToString());
            // DateTime date = person.BirthDay.Value;
            IdentidyControlClass IC = new IdentidyControlClass();
            if (IC.IdentidyControl(long.Parse(person.IdentificationNumber), person.BirthDay.Value.Year, person.Fullname) == true)
            {
                
                db.Person.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Error = "Kimlik Doğrulama Hatalı Lütfen Bilgilerinizi Kontrol Ediniz.";
                return View();
            }
             
        }
        public ActionResult PersonEdit()
        {
            List<Person> person = db.Person.ToList();
            return View(person); 
        }
        [HttpPost]
        public ActionResult PersonDelete(int Id)
        {
            Person person = db.Person.FirstOrDefault(x => x.Id == Id);
            db.Person.Remove(person);
            db.SaveChanges();
            return RedirectToAction("PersonEdit");
        }
        [HttpPost]
        public ActionResult PersonUpdate(int Id)
        {
            Person person = db.Person.FirstOrDefault(x => x.Id == Id);
            return View(person);
        }
        [HttpPost]
        public ActionResult PersonUpdateConfirm(Person person)
        {
            Person p = db.Person.Find(person.Id);
            p.IdentificationNumber = person.IdentificationNumber;
            p.Fullname = person.Fullname;
            p.Email = person.Email;
            p.CompanyName = person.CompanyName;
            p.CompanyPhoneNumber = person.CompanyPhoneNumber;
            p.Address = person.Address;
            p.BirthDay = person.BirthDay;
            db.SaveChanges();            
            return RedirectToAction("PersonEdit");
        }
    }
}