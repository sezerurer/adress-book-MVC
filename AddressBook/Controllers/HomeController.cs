﻿using AddressBook.Models;
using AddressBook.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddressBook.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        Export export = new Export();
        AddressBookEntities db = new AddressBookEntities();
        public ActionResult Index()
        {
            List<Person> person = db.Person.ToList();
            return View(person);
        }
        public void DownloadCsv()
        {
            IList<Person> person = db.Person.ToList();
            string personCsv = export.GetCsvString(person);
            // Return the file content with response body. 
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment;filename=Kişiler.csv");
            Response.Write(personCsv);
            Response.End();
        }
        public void ExportListFromTsv()
        {

            IList<Person> person = db.Person.ToList();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Kişiler.xls");
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            export.WriteTsv(person, Response.Output);
            Response.End();
        }
        public ActionResult ExportToWord()
        {
            // get the data from database
            var data = db.Person.ToList();
            // instantiate the GridView control from System.Web.UI.WebControls namespace
            // set the data source
            GridView gridview = new GridView();
            gridview.DataSource = data;
            gridview.DataBind();

            // Clear all the content from the current response
            Response.ClearContent();
            Response.Buffer = true;
            // set the header
            Response.AddHeader("content-disposition", "attachment; filename = Kişiler.doc");
            Response.ContentType = "application/ms-word";
            Response.Charset = "";
            // create HtmlTextWriter object with StringWriter
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // render the GridView to the HtmlTextWriter
                    gridview.RenderControl(htw);
                    // Output the GridView content saved into StringWriter
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            return View();
        }
    }
}