﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AddressBook.Utils
{
    public class BaseController : System.Web.Mvc.Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (Session["Email"] == null)
            {
                filterContext.Result = new RedirectResult("~/User/Login");
            }
            base.OnActionExecuted(filterContext);
        }
    }
}