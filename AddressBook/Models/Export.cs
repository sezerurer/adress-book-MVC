﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AddressBook.Models
{
    public class Export
    {
        AddressBookEntities db = new AddressBookEntities();


        public string GetCsvString(IList<Person> person)
        {
            StringBuilder csv = new StringBuilder();

            csv.AppendLine("Adı Soyadı,Kimlik No, Telefon Numarası, Doğum Tarihi, Mail, Adres, Şirket İsmi, Şirket Telefonu");

            foreach (Person per in person)
            {
                csv.Append(per.Fullname + ",");
                csv.Append(per.IdentificationNumber + ",");
                csv.Append(per.PhoneNumber + ",");
                csv.Append(per.BirthDay + ",");
                csv.Append(per.Email + ",");
                csv.Append(per.Address + ",");
                csv.Append(per.CompanyName + ",");
                csv.Append(per.CompanyPhoneNumber);


                csv.AppendLine();
            }

            return csv.ToString();
        }

        public void WriteTsv<T>(IEnumerable<T> data, TextWriter output)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor prop in props)
            {
                if (prop.DisplayName != "Id" && prop.DisplayName != "UserId" && prop.DisplayName != "Users")
                {
                    switch (prop.DisplayName)
                    {
                        case "Fullname":
                            output.Write("Adı Soyadı"); // header
                            output.Write("\t");
                            break;
                        case "IdentificationNumber":
                            output.Write("Kimlik No"); // header
                            output.Write("\t");
                            break;
                        case "BirthDay":
                            output.Write("Doğum Tarihi"); // header
                            output.Write("\t");
                            break;
                        case "PhoneNumber":
                            output.Write("Telefon No"); // header
                            output.Write("\t");
                            break;
                        case "Address":
                            output.Write("Adres"); // header
                            output.Write("\t");
                            break;
                        case "Email":
                            output.Write("Mail"); // header
                            output.Write("\t");
                            break;
                        case "CompanyName":
                            output.Write("Şirket İsmi"); // header
                            output.Write("\t");
                            break;
                        case "CompanyPhoneNumber":
                            output.Write("Şirket Telefonu"); // header
                            output.Write("\t");
                            break;
                        default:
                            break;
                    }

                }

            }
            output.WriteLine();
            foreach (T item in data)
            {
                foreach (PropertyDescriptor prop in props)
                {
                    if (prop.Name != "Id" && prop.Name != "UserId" && prop.DisplayName != "Users")
                    {
                        output.Write(prop.Converter.ConvertToString(
                                                 prop.GetValue(item)));
                        output.Write("\t");
                    }

                }
                output.WriteLine();
            }
        } 
    }
}