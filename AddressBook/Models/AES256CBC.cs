﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace AdresKayitMVC.Models
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class AES256CBC
    {
        string KeyString = "siaARexliqzOusdK0giJO8YKMWVJp/Do";
        string IVString = "9BEA053953D1F6CD";
        public string EncryptStringToBytes_Aes(string plainText)
        {
            byte[] textBoyutu = System.Text.UTF8Encoding.UTF8.GetBytes(plainText);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.Key = System.Text.UTF8Encoding.UTF8.GetBytes(KeyString);
            aes.IV = System.Text.UTF8Encoding.UTF8.GetBytes(IVString);
          //  aes.Padding = PaddingMode.;
            aes.Mode = CipherMode.CBC;
            ICryptoTransform crypto = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] sifreliMetin = crypto.TransformFinalBlock(textBoyutu, 0, textBoyutu.Length);
            crypto.Dispose();
            return Convert.ToBase64String(sifreliMetin);

        }

        public string DecryptStringFromBytes_Aes(string plainText)
        {
            byte[] sifreliMetiBoyutu = Convert.FromBase64String(plainText);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.Key = System.Text.UTF8Encoding.UTF8.GetBytes(KeyString);
            aes.IV = System.Text.UTF8Encoding.UTF8.GetBytes(IVString);
           // aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            ICryptoTransform crypto = aes.CreateDecryptor(aes.Key, aes.IV);
            byte[] metin = crypto.TransformFinalBlock(sifreliMetiBoyutu, 0, sifreliMetiBoyutu.Length);
            crypto.Dispose();
            return System.Text.ASCIIEncoding.UTF8.GetString(metin);
        }
    }
}